﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController_Level2 : MonoBehaviour {

	float currentTime;
	float startTime;
	public Text monolog; 
	bool currentlyTalking;
	int monologLength;

	PlayerController playercontrollerscript;
	bool setNew;

	// Use this for initialization
	void Start () {
        setStartState();
		//Einführungstext
		interact("start", "none", false);
		currentlyTalking = true;
        findAndSetAllObjects();
		setNew = true;
	
	}

    private void setStartState()
    {
        currentTime = 0f;
        startTime = 0f;
        monolog.text = "";
        currentlyTalking = false;
    }

    private void findAndSetAllObjects()
    {
        playercontrollerscript = gameObject.GetComponent<PlayerController>();
        
    }

    // Update is called once per frame
    void Update () {
        manageTextAppearance();
	}

    private void manageTextAppearance()
    {
        currentTime += Time.deltaTime;
        if (currentTime > (startTime + 0.1f * monologLength - 0.5f) && currentlyTalking == true)
        {
            monolog.color = new Color(1.0f, 1.0f, 1.0f, monolog.color.a - Time.deltaTime * 1.5f);
        }
        if (currentTime > startTime + 0.1f * monologLength && currentlyTalking == true)
        {
            monolog.text = "";
            startTime = 0f;
            currentlyTalking = false;
            monolog.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        }
        if (currentTime < (startTime + 1.0f) && currentlyTalking == true && monolog.color.a < 1.0f)
        {
            monolog.color = new Color(1.0f, 1.0f, 1.0f, monolog.color.a + Time.deltaTime);
        }
    }

    public void interact(string objectName, string carryingObjectName, bool down){
		setNew = true;
        switch (objectName)
        {
            case "start":
                monolog.text = "Level 2";
                break;
            default:
                Debug.Log("Interaktion ist schiefgelaufen");
                break;
        }
        startNewMonologIfNeeded();
	}

    private void startNewMonologIfNeeded()
    {
        if (setNew)
        {
            monologLength = monolog.text.Length;
            if (monolog.text.Length < 20)
                monologLength = 20;
            if (monolog.text.Length > 60)
                monologLength = 50;
            startTime = currentTime;
            currentlyTalking = true;
            monolog.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        }
    }

    
    private void handleDialogDependingOnPlayerItem(string carryingObjectName, bool down)
    {
       
    }


	void triggerWithoutInteract(string objectName, string carryingObjectName){

	}
    
}