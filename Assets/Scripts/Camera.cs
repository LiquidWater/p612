﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

	public Transform player;
	UnityEngine.Camera cam;
	float borderRight;
	float borderLeft;
	float halfCameraView;
	float cameraLeftEdge;
	float cameraRightEdge;

	bool zoom = true;
	bool beforezoom;
	float zoomStart=5f;
	float zoomTime =0f;
	float zoomDuration = 5f;
	float biggerSize;
	float smallerSize =16f;
	float sizeDifferenz;

	void Start () {
		setCameraEdges();
		if (zoom == true) {
			biggerSize=cam.orthographicSize;
			sizeDifferenz = biggerSize - smallerSize;
			beforezoom = true;
		}
	}
	
	void Update () {
		transform.position = new Vector3 (Mathf.Clamp(player.position.x, cameraLeftEdge, cameraRightEdge), -5, -10); 
		if (zoom == true) {
			if (beforezoom == true) {
				zoomTime += Time.deltaTime;
				if (zoomTime >= zoomStart) {
					beforezoom = false;
					zoomTime = 0f;
				}
			} else {
				zoomTime += Time.deltaTime;
				cam.orthographicSize -= sizeDifferenz*(Time.deltaTime/zoomDuration);
				setCameraEdges();
				if(zoomTime >= zoomDuration) {
					cam.orthographicSize = 16f;
					zoom = false;
					Debug.Log("zoom done");
				}
			}
		}
	}

	private void setCameraEdges(){
		cam = UnityEngine.Camera.main;
		halfCameraView = cam.orthographicSize * Screen.width / Screen.height;
		borderRight = GameObject.Find("BorderRight").transform.position.x - 2;
		borderLeft = GameObject.Find("BorderLeft").transform.position.x + 2;
		cameraLeftEdge = borderLeft + halfCameraView - halfCameraView / 10;
		cameraRightEdge = borderRight - halfCameraView + halfCameraView / 10;
	}


}
