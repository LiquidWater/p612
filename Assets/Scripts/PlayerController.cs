﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour {

	public Animator anim;
	public Animator armAnim;

	public Transform carryLocation;
	public Transform floorLocation;

	GameObject currentCarryingObject;
	Collider2D currentCollider;
	Collider2D[] arrayCurrentColliders = new Collider2D[10];

	private float Speed;	
	private bool nowOnTrigger;
	float borderRight;
	float borderLeft;
	//private string characterPosition;
	UIController uicontrollerscript;

	int highestSetOrder =0;

	float cTime= 0f;

    //Ladder stuff
    private bool onLadderTrigger = false;
    private float groundLevel;

    Vector2 originalObjectSize;
	Vector2 originalObjectOffset;

	void Start () {
		uicontrollerscript = gameObject.GetComponent<UIController>();
		borderRight = GameObject.Find("BorderRight").transform.position.x - 2;
		borderLeft = GameObject.Find("BorderLeft").transform.position.x + 2;
        groundLevel = transform.position.y;
	}

	void Update () {
		handleKeyBoardInput ();
		cTime += Time.deltaTime;
		Speed = 5f * Time.deltaTime;

	}

	private void handleKeyBoardInput() {
		//Leiter hochklettern möglich wenn on Ground oder wenn nicht
		if (Input.GetKey(KeyCode.UpArrow) && onLadderTrigger){
			climbLadderUp();
			anim.speed = 1f;
		}

		if (transform.position.y != groundLevel) {
			//Leiter runter klettern wenn Down
			if (Input.GetKey (KeyCode.DownArrow)) {
				climbLadderDown ();
				anim.speed = 1f;
			}
			//wenn auf Leiter aber nicht bewegt
			if (!(Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.UpArrow))) {
				anim.speed = 0f;
			}
		} else { //on Ground
			
			if (!leftAndRightPressed()){ //Damit man wenn man gelichzeitig drückt stehen bleibt

				//interagieren
				if (Input.GetKeyDown (KeyCode.UpArrow)){
					interact ();
				}

				//absetzen
				if (Input.GetKeyDown (KeyCode.DownArrow)){
					putDown();
				}

				//Laufen
				if (Input.GetKey (KeyCode.RightArrow) && transform.position.x < borderRight && transform.position.y == groundLevel){
					walkRight();
				}

				else if(Input.GetKey (KeyCode.LeftArrow) && transform.position.x > borderLeft && transform.position.y == groundLevel) {
					walkLeft();
				}
				else {
					anim.SetBool ("Laufen", false);
					armAnim.SetBool ("Laufen", false);
				}
			} else {
				anim.SetBool("Laufen", false);
				armAnim.SetBool ("Laufen", false);
			}
		}
		
      
	}

    private void climbLadderUp() {
        transform.position += new Vector3 (0, 2 * Speed, 0);
		transform.eulerAngles = new Vector3(0, 0, 0);
		anim.SetBool ("Klettern", true);
    }

    private void climbLadderDown() {
        //Harte Zeiten erfordern harte Maßnahmen
        if (groundLevel - transform.position.y > -0.3) {
            transform.position = new Vector3(transform.position.x, groundLevel, transform.position.z);
			anim.SetBool ("Klettern", false);
        } else{
            transform.position += new Vector3(0, -2 * Speed, 0);
            transform.eulerAngles = new Vector3(0, 0, 0);
			anim.SetBool ("Klettern", true);
        }
    }

    private void walkRight(){
		transform.position += new Vector3 (1 * Speed, 0, 0);
		transform.eulerAngles = new Vector3(0, 0, 0);
		anim.SetBool ("Laufen", true);
		armAnim.SetBool ("Laufen", true);
	}

	private void walkLeft(){
		transform.position += new Vector3 (-1 * Speed, 0, 0);		
		transform.eulerAngles = new Vector3(0, 180, 0);
		anim.SetBool ("Laufen", true);
		armAnim.SetBool ("Laufen", true);
	}

	private void interact(){
		if (currentCarryingObject == null) {
			if (nowOnTrigger && currentCollider != null) {
				uicontrollerscript.interact (currentCollider.name, "none", false);
				if (currentCollider.name.Substring (0, 3) == "obj") {
					pickUpObject ();
				} else {
					armAnim.SetTrigger ("Interagiert");
				}
			} else {
				armAnim.SetTrigger ("Interagiert");
			}
		} else {
			if (nowOnTrigger && currentCollider != null)
				uicontrollerscript.interact (currentCollider.name, currentCarryingObject.name, false);
		}
	}

	void putDown(){
		if (currentCarryingObject != null) {
			if(currentCollider != null) uicontrollerscript.interact (currentCollider.name, currentCarryingObject.name, true);
			setDownObject ();
		} else {
			if (nowOnTrigger == true && currentCollider != null)
				uicontrollerscript.interact (currentCollider.name, "none", true);
			armAnim.SetTrigger ("Interagiert");
		}
	}

	private bool leftAndRightPressed(){
		return Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftArrow);
	}

	void OnTriggerEnter2D(Collider2D coll){
        if(coll.name != "plokamia_Leiter") {
            nowOnTrigger = true;
            addToColliderArray(coll);
            setOrderInColliderArray();
            currentCollider = arrayCurrentColliders[0];
        } else {
			if(currentCarryingObject == null)
            onLadderTrigger = true;
        }


		//falls Reinlaufen alleine schon Text triggern soll

		if (coll.name.Substring (0, 3) == "tr_") {
			if(currentCarryingObject!= null)
				uicontrollerscript.interact (coll.name, currentCarryingObject.name, true);
			else
				uicontrollerscript.interact (coll.name, "none", true);
		}


	}

	void OnTriggerExit2D(Collider2D coll){
        if(coll.name == "plokamia_Leiter") {
            onLadderTrigger = false;
        } else {
            deleteFromColliderArray(coll);
            setOrderInColliderArray();
            currentCollider = arrayCurrentColliders[0];
        }
	}

	void setSortingOrderonGround(){
		if (arrayCurrentColliders[0] != null) {
			currentCarryingObject.GetComponent<SpriteRenderer>().sortingOrder = arrayCurrentColliders[0].gameObject.GetComponent<SpriteRenderer> ().sortingOrder + 1;
		}
		else
			currentCarryingObject.GetComponent<SpriteRenderer>().sortingOrder = 0;
	}

	void pickUpObject(){
        setCurrentCarryingObjectDataRelativeToPlayer();
        setItemCorrectlyIfReverse();
        deleteFromColliderArray(currentCarryingObject.GetComponent<Collider2D>());
        setOrderInColliderArray();
        currentCollider = arrayCurrentColliders[0];
        setAnimationForArmAndPlayerDependingOnPickUpOrSetDown(true);
        setObjectSizeAndOffset();
	}

    private void setObjectSizeAndOffset()
    {
        float objectSize = currentCarryingObject.GetComponent<BoxCollider2D>().size.x / 2;
        gameObject.GetComponent<BoxCollider2D>().size += new Vector2(objectSize, 0f);
        gameObject.GetComponent<BoxCollider2D>().offset += new Vector2(objectSize / 2 + 0.5f, 0f);
    }

    private void setAnimationForArmAndPlayerDependingOnPickUpOrSetDown(bool v)
    {
        anim.SetBool("Carry", v);
        armAnim.SetBool("Carry", v);
    }

    private void setItemCorrectlyIfReverse()
    {
        //Sodass bei Revert Tragen auch das Object noch gut liegt, nur für Objecte mit "rev"
        if (currentCarryingObject.name.Substring(4, 3) == "rev")
        {
            if (gameObject.transform.eulerAngles.y != currentCarryingObject.transform.eulerAngles.y)
            {
                if (currentCarryingObject.transform.eulerAngles.y == 180) currentCarryingObject.transform.eulerAngles = new Vector3(0, 0, 0);
                else currentCarryingObject.transform.eulerAngles = new Vector3(0, 180, 0);
            }
        }
    }

    private void setCurrentCarryingObjectDataRelativeToPlayer() {
        currentCarryingObject = currentCollider.gameObject;
        currentCarryingObject.GetComponent<Collider2D>().enabled = false;
        currentCarryingObject.transform.position = carryLocation.position;
        currentCarryingObject.transform.parent = transform;
        currentCarryingObject.GetComponent<SpriteRenderer>().sortingLayerName = "carryObject";
    }

    public void setDownObject(){
		currentCarryingObject.GetComponent<SpriteRenderer> ().sortingLayerName = "objects";
		setSortingOrderonGround();
		currentCarryingObject.GetComponent<Collider2D>().enabled = true;
		gameObject.GetComponent<CapsuleCollider2D> ().size -= currentCarryingObject.GetComponent<BoxCollider2D> ().size;
		currentCarryingObject.transform.position = floorLocation.position;
		currentCarryingObject.transform.parent = null;
		gameObject.GetComponent<BoxCollider2D> ().size = new Vector2 (2.28f,4.52f);
		gameObject.GetComponent<BoxCollider2D> ().offset = new Vector2 (-0.62f,-0.31f);
		currentCarryingObject = null;
        setAnimationForArmAndPlayerDependingOnPickUpOrSetDown(false);
	}

	void addToColliderArray(Collider2D coll){
		for (int i = 0; i < 10; i++) {
			if (arrayCurrentColliders[i] == null) {
				arrayCurrentColliders[i] = coll;
				i = 10;
			}
		}
	}

	void deleteFromColliderArray(Collider2D coll){
		for (int i = 0; i < 10; i++) {
			if (arrayCurrentColliders [i]!=null && arrayCurrentColliders[i].name == coll.name) {
				arrayCurrentColliders [i] = null;
				i = 10;
			}
		}

	}

	void setOrderInColliderArray(){
		
		Collider2D[] zwischenArray = new Collider2D[10];
		int highest;

		for (int i = 0; i < 10; i++) {
			highest = findHighestSortingOrderInArray ();
			if(arrayCurrentColliders[highest] != null){
				
				bool alreadyInArray = false;
				for(int i2=0; i2<10; i2++){
					if (zwischenArray[i2] != null && zwischenArray[i2].name == arrayCurrentColliders[highest].name){
						alreadyInArray = true;
					}
				}
				if (!alreadyInArray)
					zwischenArray [i] = arrayCurrentColliders [highest];
				else
					i -= 1;
			
				arrayCurrentColliders[highest] = null;
			}
		}
		arrayCurrentColliders = zwischenArray;
	}

	int findHighestSortingOrderInArray(){
		int highestOrder=0;
		int high=0;

		for (int i = 0; i < 10; i++) {
			if (arrayCurrentColliders [i] != null && arrayCurrentColliders [i].gameObject.GetComponent<SpriteRenderer> ().sortingOrder >= highestOrder) {
				high = i;
				highestOrder = arrayCurrentColliders [i].gameObject.GetComponent<SpriteRenderer> ().sortingOrder;
			}
		}
		return high;
	}
}
