﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fading : MonoBehaviour {

	public Texture2D fadeOutTexture;
	public float fadeSpeed = 0.8f;
	private int drawDepth = -1000;
	private float fadeAlpha = 1.0f;
	private int fadeDirection = -1;

	void OnGUI(){
		fadeAlpha += fadeDirection * fadeSpeed * Time.deltaTime;
		fadeAlpha = Mathf.Clamp01 (fadeAlpha);

		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, fadeAlpha);
		GUI.depth = drawDepth;
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), fadeOutTexture);
	}

	public float beginFade(int direction){
		fadeDirection = direction;
		return(fadeSpeed);
	}
	void OnLevelWasLoaded(){
		beginFade (-1);
	}
}
