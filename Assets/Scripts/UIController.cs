﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

	float currentTime;
	float startTime;
	public Text monolog; 
	bool currentlyTalking;
	int monologLength;
	string kabelZustand ="kaputt";
	string schuppenZustand = "verschlossen";
	string heilesKabelZustand = "heil";
	GameObject beil;
	GameObject schere;
	GameObject klebeband;
	GameObject metallstueck;
	GameObject draht;
	GameObject schuppen_offen;
	GameObject schuppen_geschlossen;
	GameObject ende_rechts;
	public Animator kabelHeilAnim;
	public Animator kabelKaputtAnim;
	public Animator lichtAnim;
	PlayerController playercontrollerscript;
	bool setNew;
	string oldMonolog ="";

	// Use this for initialization
	void Start () {
        setStartState();
		//Einführungstext
		interact("start", "none", false);
		currentlyTalking = true;
        findAndSetAllObjects();
		setNew = true;
	
	}

    private void setStartState()
    {
        currentTime = 0f;
        startTime = 0f;
        monolog.text = "";
        currentlyTalking = false;
    }

    private void findAndSetAllObjects()
    {
        playercontrollerscript = gameObject.GetComponent<PlayerController>();
        metallstueck = GameObject.Find("obj_rev_Metallstueck");
        schuppen_geschlossen = GameObject.Find("Schuppen_geschlossen");
        schuppen_offen = GameObject.Find("Schuppen_offen");
        schuppen_offen.SetActive(false);
        beil = GameObject.Find("obj_rev_Beil");
        beil.SetActive(false);
        schere = GameObject.Find("obj_rev_schere");
        schere.SetActive(false);
        klebeband = GameObject.Find("obj_Klebeband");
        klebeband.SetActive(false);
        draht = GameObject.Find("obj_rev_Draht");
        draht.SetActive(false);
        ende_rechts = GameObject.Find("tr_Ende_rechts");
    }

    // Update is called once per frame
    void Update () {
        manageTextAppearance();
	}

    private void manageTextAppearance()
    {
        currentTime += Time.deltaTime;
        if (currentTime > (startTime + 0.1f * monologLength - 0.5f) && currentlyTalking == true)
        {
            monolog.color = new Color(1.0f, 1.0f, 1.0f, monolog.color.a - Time.deltaTime * 1.5f);
        }
        if (currentTime > startTime + 0.1f * monologLength && currentlyTalking == true)
        {
            monolog.text = "";
            startTime = 0f;
            currentlyTalking = false;
            monolog.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        }
        if (currentTime < (startTime + 1.0f) && currentlyTalking == true && monolog.color.a < 1.0f)
        {
            monolog.color = new Color(1.0f, 1.0f, 1.0f, monolog.color.a + Time.deltaTime);
        }
    }

    public void interact(string objectName, string carryingObjectName, bool down){
		setNew = true;
        switch (objectName)
        {
            case "start":
                monolog.text = "Wo... Was ist hier los? Wo ist mein Volk? Die sonst belebten Straßen sind leer...";
                break;
            case "vieleLaternen":
                if (carryingObjectName == "none" || !down)
                    monolog.text = "Die Laternen sind ganz verwildert... Als hätte sie seit Jahren niemand gestutzt!";
                break;
            case "Schuppen_geschlossen":
                handleDialogDependingOnShedState(carryingObjectName, down);
                break;
            case "Kabel_3_interact":
                handleDialogDependingOnPlayerItem(carryingObjectName, down);
                break;
            case "obj_rev_Metallstueck":
                if (!down)
                    monolog.text = "Ein altes Stück Metall... Es scheint so, als würde sich keiner mehr um Plokamia kümmern.";
                else
                    setNew = false;
                break;
            case "obj_rev_Beil":
                if (!down)
                    monolog.text = "Eine Axt...";
                else
                    setNew = false;
                break;
            case "obj_rev_schere":
                if (!down)
                    monolog.text = "Eine Schere...";
                else
                    setNew = false;
                break;
            case "obj_Klebeband":
                if (!down)
                    monolog.text = "Klebeband...";
                else
                    setNew = false;
                break;
            case "obj_rev_Draht":
                if (!down)
                    monolog.text = "Alter Draht... ob ich den wieder verwenden kann?";
                break;
            case "Rakete_leer":
                if (!(carryingObjectName != "none" && down))
                    monolog.text = "Warum wegfliegen, wenn man nicht bei der Rückkehr umarmt wird?";
                break;
            case "Kabel_2_kaputt":
                handleCableDialogDependingOnPlayerItem(carryingObjectName, down);
                break;
            case "tr_Ende_rechts":
                monolog.text = "Es ist viel zu dunkel, ich sehe nicht mal meine eigenen Füße! Warte... habe ich Füße?";
                break;
            case "tr_Ende_links":
                monolog.text = "Ich möchte lieber nach Hause gehen... Vielleicht gibt es eine Überraschungs-Rückkehr-Feier?";
                break;
		case "tr_GoToSecondLevel":
			StartCoroutine(goToSecondLevel());
			break;
            default:
                Debug.Log("Interaktion ist schiefgelaufen");
                break;
        }
		setNew = checkIfTextIsNew ();
        startNewMonologIfNeeded();
	}

    private void startNewMonologIfNeeded()
    {
        if (setNew)
        {
            monologLength = monolog.text.Length;
            if (monolog.text.Length < 20)
                monologLength = 20;
            if (monolog.text.Length > 60)
                monologLength = 50;
            startTime = currentTime;
            currentlyTalking = true;
            monolog.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
			oldMonolog = monolog.text;
        }
    }

    private void handleCableDialogDependingOnPlayerItem(string carryingObjectName, bool down)
    {
        if (carryingObjectName == "none")
        {
            monolog.text = "Das Kabel ist ganz verfressen. Ob das gefährlich ist?";
        }
        if (carryingObjectName == "obj_rev_Draht")
        {
            if (down == false)
            {
                playercontrollerscript.setDownObject();
            }
            if (kabelZustand == "kaputt")
            {
                monolog.text = "Verbunden! Aber noch offen und gefährlich...";
                GameObject.Find(carryingObjectName).SetActive(false);
                kabelZustand = "verbunden";
                kabelKaputtAnim.SetBool("draht_heilen", true);
            }
        }
        if (carryingObjectName == "obj_Klebeband")
        {
            if (kabelZustand == "kaputt" && down == false)
                monolog.text = "Ich bin kein Physiker... aber Klebeband leitet keinen Strom.";
            if (kabelZustand == "verbunden")
            {
                if (down == false)
                {
                    playercontrollerscript.setDownObject();
                }
                monolog.text = "Alles repariert!";
                GameObject.Find(carryingObjectName).SetActive(false);
                kabelZustand = "repariert";
                kabelKaputtAnim.SetBool("umwickeln", true);
                ende_rechts.SetActive(false);
                lichtAnim.SetBool("kabel_repariert", true);
            }
        }
        if (carryingObjectName == "obj_rev_Beil")
        {
            if (down == false)
                monolog.text = "Kaputtes noch kaputter machen?";
        }
        if (carryingObjectName == "obj_rev_schere")
        {
            if (down == false) monolog.text = "Wir such alle nach Verbindungen... nicht nach Abschnitten.";
        }
    }

    private void handleDialogDependingOnPlayerItem(string carryingObjectName, bool down)
    {
		if (heilesKabelZustand == "heil")
        {
			
            monolog.text = "Überall Kabel... Wenn sie zu schnell wachsen können sie ganze Häuser schief stellen.";
			if (carryingObjectName == "obj_rev_schere" && heilesKabelZustand == "heil")
			{
				if (down == false)
				{
					playercontrollerscript.setDownObject();
				}
				monolog.text = "Schnipp Schnapp!";
				kabelHeilAnim.SetBool("kaputt_machen", true);
				draht.SetActive(true);
				heilesKabelZustand = "kaputt";
			}
			if (carryingObjectName == "obj_Klebeband") {
				monolog.text = "Ich könnte alles Mögliche aneinander festkleben, aber nicht alles ist sinnvoll.";
			}
			if (carryingObjectName == "obj_rev_Beil") {
				monolog.text = "Feingefühl würde mir mehr nutzen...";
			}
        }
		if (heilesKabelZustand == "kaputt") {
			monolog.text = "Manchmal muss man Geliebtes loslassen um weiter zu kommen...";
		}
    }

    private void handleDialogDependingOnShedState(string carryingObjectName, bool down)
    {
        if (schuppenZustand == "verschlossen")
        {
            if (carryingObjectName == "none")
            {
                monolog.text = "Der Schuppen des Gärtners... Verschlossen.";
            }
            else
            {
                monolog.text = "Das bringt glaube ich nichts.";

            }
            if (carryingObjectName == "obj_rev_Metallstueck")
            {

                if (down == false)
                {
                    playercontrollerscript.setDownObject();
                }

                monolog.text = "Aufbruch! Tut mir leid, lieber Gärtner... aber ich brauche deine Geräte!";
                beil.SetActive(true);
                schere.SetActive(true);
                klebeband.SetActive(true);
                metallstueck.SetActive(false);
                schuppenZustand = "offen";
                schuppen_offen.SetActive(true);
                schuppen_geschlossen.SetActive(false);
            }
        }
    }

	IEnumerator goToSecondLevel(){
		Debug.Log("Go To Second Level");
		float fadeTime = GameObject.Find ("tr_GoToSecondLevel").GetComponent<Fading>().beginFade(1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene("Level_2");
	}

	bool checkIfTextIsNew(){

		if (currentlyTalking == true && monolog.text == oldMonolog)
			return false;
		else
			return true;
	}

}